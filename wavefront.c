/*
 * raytrace by matthilde
 *
 * wavefront.c
 *
 * Wavefront .OBJ loader and parser for the raytracer
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "raytrace.h"

#define MATCH(a,b) (strcmp(a, b) == 0)
#define AB2VEC(ab) ((vec3){ atof(ab.args[0]), \
                            atof(ab.args[1]), \
                            atof(ab.args[2]) })
#define PARSETRIARG(s,v,n) (sscanf(s, "%d//%d", &v, &n))

// Used to store parsed information
typedef struct Argbuf {
    enum { OBJ_V, OBJ_VN, OBJ_F } type;
    char args[3][16];
} Argbuf;

// Calculates the face normal from vertex normals
static vec3 get_face_normal(vec3 *vertices, vec3 *normals) {
    vec3 p0 = vec3_sub(vertices[1], vertices[0]);
    vec3 p1 = vec3_sub(vertices[2], vertices[0]);
    vec3 fn = vec3_normalize(vec3_crossp(p0, p1));

    // vec3 vn = vec3_add(normals[0], vec3_add(normals[1], normals[2]));
    // vn = vec3_fdiv(vn, 3.0);
    vec3  vn  = vertices[0];
    float dot = vec3_dotp(fn, vn);

    if (dot < 0.0)
        return vec3_fmul(fn, -1.0);
    else
        return fn;
}

// Takes maximum 3 arguments
// Returns the number of provided arguments
// Returns 0 if invalid, returns -1 on EOF
static int parse_one_line(FILE *f, Argbuf *argbuf) {
    char line[256]; char *s, cmd[16]; int l;
    // char **buf = (char **)argbuf->args;
    char buf[3][16];

    s = fgets(line, 256, f);
    if (!s) return -1;

    l = sscanf(line, "%s%s%s%s", cmd, buf[0], buf[1], buf[2]);
    if (l == -1 || l == 1) return 0;

    strcpy(argbuf->args[0], buf[0]);
    strcpy(argbuf->args[1], buf[1]);
    strcpy(argbuf->args[2], buf[2]);

    if (MATCH(cmd, "v")) {
        argbuf->type = OBJ_V; }
    else if (MATCH(cmd, "vn"))
        argbuf->type = OBJ_VN;
    else if (MATCH(cmd, "f"))
        argbuf->type = OBJ_F;
    else
        return 0;
    return l - 1;
}

int errorthing = -1;

#define FAIL(x) ({errorthing=x;free(vertices);free(normals);free(triangles);return -1;})
static size_t get_vertices_triangles(FILE *f,
                                     vec3 **vecptr, size_t *vsz,
                                     Triangle **facesptr) {
    Argbuf argbuf; int l; size_t vsize = 0, fsize = 0, nsize = 0;
    vec3 *normals, *vertices; Triangle *triangles;

    // First pass: Get number of vertices, normals and faces
    fseek(f, 0, SEEK_SET);
    while ((l = parse_one_line(f, &argbuf)) != -1) {
        if (l)
            switch (argbuf.type) {
            case OBJ_V:  ++vsize; break; // vertices
            case OBJ_VN: ++nsize; break; // normals
            case OBJ_F:  ++fsize; break; // triangles/faces
            }
    }

    vertices  = (vec3*)malloc(sizeof(vec3) * vsize);
    if (!vertices) return -1;
    normals   = (vec3*)malloc(sizeof(vec3) * nsize);
    if (!normals) {
        free(vertices); return -1;
    }
    triangles = (Triangle*)malloc(sizeof(Triangle) * fsize);
    if (!triangles) {
        free(vertices); free(normals); return -1;
    }

    // Second pass : Reading information from vertices and normals
    fseek(f, 0, SEEK_SET);
    size_t vi, ni, fi;
    vi = ni = fi = 0;
    while ((l = parse_one_line(f, &argbuf)) != -1) {
        if (l)
            switch (argbuf.type) {
            case OBJ_V:
                if (l != 3) FAIL(1);
                vertices[vi++] = AB2VEC(argbuf);
                break;
            case OBJ_VN:
                if (l != 3) FAIL(2);
                normals[ni++] = AB2VEC(argbuf);
                break;
            }
    }

    // Third pass : Faces
    fseek(f, 0, SEEK_SET);
    Triangle* curface;
    vec3 facevers[3], vernorms[3]; int vid, nid;
    
    while ((l = parse_one_line(f, &argbuf)) != -1) {
        if (l && argbuf.type == OBJ_F) {
            if (l != 3) FAIL(3);
            // Get current face struct to fill
            curface = (triangles + (fi++));
            // Get face vertices and vertices normals
            for (int i = 0; i < 3; ++i) {
                if (PARSETRIARG(argbuf.args[i], vid, nid) != 2)
                    FAIL(4);
                facevers[i] = vertices[vid - 1];
                vernorms[i] = normals[vid - 1];
            }
            // Populate triangle vertices
            curface->a = facevers[0];
            curface->b = facevers[1];
            curface->c = facevers[2];
            // Calculate face normal
            curface->normal = get_face_normal(facevers, vernorms);
        }
    }

    free(normals);
    *facesptr = triangles;
    *vecptr = vertices;
    *vsz = vsize;
    return fsize;
}

int wavefront_load(const char* filename, Mesh* m, vec3 pos) {
    vec3 *vertices; Triangle* triangles;
    size_t vsz, tsz;

    m->pos = pos;

    FILE *f = fopen(filename, "r");
    if (!f) return 0;

    tsz = get_vertices_triangles(f, &vertices, &vsz, &triangles);
    free(vertices); // why did i put that in the first place even...
    if (tsz == -1) return 0;

    for (size_t i = 0; i < tsz; ++i) {
        triangles[i].a = vec3_add(triangles[i].a, pos);
        triangles[i].b = vec3_add(triangles[i].b, pos);
        triangles[i].c = vec3_add(triangles[i].c, pos);
    }

    m->triangles = triangles;
    m->nt = tsz;
    return 1;
}

void wavefront_free_mesh(Mesh* m) {
    free(m->triangles);
    if (m->materials) free(m->materials);
}

#ifdef DEBUG
#define PRINTVEC(v) (printf("(%lf %lf %lf)\n", v.x, v.y, v.z))

int main(int argc, char **argv) {
    if (argc != 2) return 1;
    Mesh m;

    printf("Loading %s...\n", argv[1]);
    int success = wavefront_load(argv[1], &m);
    if (!success) {
        printf("Loading .obj file has failed. (%d)\n", errorthing);
        return 1;
    }

    printf("%lu\n", m.triangles);
    printf("%d faces.\n", m.nt);
    for (size_t i = 0; i < m.nt; ++i) {
        printf("TRIANGLE %d\n----------\n", i + 1);
        printf("a      = "); PRINTVEC(m.triangles[i].a);
        printf("b      = "); PRINTVEC(m.triangles[i].b);
        printf("c      = "); PRINTVEC(m.triangles[i].c);
        printf("normal = "); PRINTVEC(m.triangles[i].normal);
    }
    return 0;
}

#endif
