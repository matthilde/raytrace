#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>

#include "raytrace.h"

// death.
void die(const char* err) {
    perror(err); exit(1);
}

// write canvas to PPM file
void ppm_write_file(const char* fn, Canvas* canvas) {
    FILE* f = fopen(fn, "w");
    Color c, *colors = canvas->cn; u8 cc[3];
    int w = canvas->w, h = canvas->h;
    F max;

    fprintf(f, "P3\n%d %d\n255\n", w, h);
    for (int y = 0; y < h; ++y)
        for (int x = 0; x < w; ++x) {
            c = colors[y*w+x];
            max = fmax(1.0, fmax(c.r, fmax(c.g, c.b)));
            cc[0] = (F)((c.r / max) * 255.0);
            cc[1] = (F)((c.g / max) * 255.0);
            cc[2] = (F)((c.b / max) * 255.0);
            fprintf(f, "%u %u %u ", cc[0], cc[1], cc[2]);
            // fprintf(f, "%u %u %u ", c.r, c.g, c.b);
        }

    fclose(f);
}

// Clear the canvas
void clear_canvas(Canvas* cn) {
    for (int i = 0; i < cn->w * cn->h; ++i)
        cn->cn[i] = (Color){0, 0, 0};
}

// Plot a point with the color you want
void plot_point(Canvas *cn, point p, Color c) {
    int w = cn->w, h = cn->h;
    if (p.x >= 0 && p.x < w && p.y >= 0 && p.y < h)
        cn->cn[p.y*w+p.x] = c;
}

////////////

// TODO: Modify compile.sh and refactor stuff

////////////

int load_obj_mesh(const char* filename, Mesh* mesh) {
    FILE *f = fopen(filename, "r");
    if (!f) die("load_obj_mesh: fopen");

    
}

////////////

// Moller-Trumbmore algorithm
int ray_triangle_intersection(Triangle* t, vec3 origin,
                              vec3 dir, F *dist) {
    F a, f, u, v;
    vec3 e1, e2, h, s, q;

    e1 = vec3_sub(t->b, t->a);
    e2 = vec3_sub(t->c, t->a);

    h = vec3_crossp(dir, e2);
    a = vec3_dotp(e1, h);
    if (fabs(a) < 0.0001) return 0;

    f = 1 / a;
    s = vec3_sub(origin, t->a);
    u = f * vec3_dotp(s, h);
    if (u < 0.0 || u > 1.0) return 0;

    q = vec3_crossp(s, e1);
    v = f * vec3_dotp(dir, q);
    if (v < 0.0 || (u + v) > 1.0) return 0;

    *dist = f * vec3_dotp(e2, q);
    if (*dist > 0.0001) return 1;
    else return 0;
}

// This function assumes a normalized vector as direction.
int ray_sphere_intersection(Sphere* s, vec3 origin, vec3 dir, F *dist) {
    F d, d0, d1;
    
    vec3 omc = vec3_sub(origin, s->pos);
    F a = vec3_dotp(dir, omc);
    F b = vec3_dotp(omc, omc) - (s->radius)*(s->radius);

    F delta = a*a - b;
    if (delta < 0) return 0;
    d0 = -a + sqrt(delta);
    d1 = -a - sqrt(delta);
    d = fmin(d0, d1);
    if (d < 0.001) return 0;
    // *point = vec3_add(origin, vec3_fmul(dir, d));
    // *normal = vec3_normalize(vec3_sub(*point, s->pos));
    *dist = d;
    return 1;
}
                            

// Get direction vector
vec3 get_dirvec(Canvas* cn, point pixel) {
    F d = cn->d;
    F w = cn->w, h = cn->h;

    vec3 r;
    r.x = ((F)pixel.x / w) - 0.5;
    r.y = 0.5 - ((F)pixel.y / h);
    r.z = d;

    return r;
}

// true if intersects.
// It uses the "bruteforce" way as in, it traces a ray and checks if
// it collides at some point.
// I know this is slow... I might change it once I decided to understand
// how ray-sphere intersection works.
int intersect(Canvas* cn, vec3 origin, vec3 dir,
              vec3* point, vec3* normal, Material* mat) {

    F maxDist = cn->maxDist, dist, closest = 1001;
    vec3 ray = origin; u8 tmp; F a;
    Sphere* spheres = cn->sph; int scount = cn->sphcount;
    vec3 p;

    for (int i = 0; i < cn->sphcount; ++i) {
        if (ray_sphere_intersection(cn->sph + i, origin, dir, &dist)) {
            if (dist < closest) {
                closest = dist;
                *mat = cn->sph[i].material;
                *point  = vec3_add(origin, vec3_fmul(dir, closest));
                *normal = vec3_normalize(vec3_sub(*point, cn->sph[i].pos));
            }
        }
    }

    Triangle* tt;
    for (int i = 0; i < cn->mcount; ++i) {
        for (int j = 0; j < cn->meshes[i].nt; ++j) {
            tt = cn->meshes[i].triangles + j;
            if (ray_triangle_intersection(tt, origin, dir, &dist)) {
                if (dist < closest) {
                    closest = dist;
                    mat->color = (Color){ 0.7, 0.0, 0.7 };
                    mat->albedo[0] = 1.0;
                    mat->albedo[1] = 0.0;
                    mat->albedo[2] = 0.0;

                    *point = vec3_add(origin, vec3_fmul(dir, closest));
                    *normal = tt->normal;
                }
            }
        }
    }

    /*
    Triangle trtr = {
        {-30.0, -10.0, 90.0}, {30.0, -10.0, 90.0}, {30.0, 20.0, 92.0},
        { {1.0, 0.4, 0.2}, { 0.5 , 0.0, 0.6 }, 1.0 }};
    if (ray_triangle_intersection(&trtr, origin, dir, &dist)) {
        if (dist < closest) {
            closest = dist;
            *mat = trtr.material;
            *point = vec3_add(origin, vec3_fmul(dir, closest));
            // *normal = (vec3){0.0, 0.0, -1.0};
            p = vec3_crossp(vec3_sub(trtr.c, trtr.a),
                            vec3_sub(trtr.b, trtr.a));
            *normal = vec3_normalize(p);
        }
    }
    */

    if (fabs(dir.y) > 0.001) {
        a = -(origin.y+10)/dir.y;
        p = vec3_add(origin, vec3_fmul(dir, a)); 
        if (a > 0.0 && a < closest) {
            p.y = -10.0;
            closest = a;
            // tmp = (u8)(p.x * 25.0) ^ (u8)(p.z * 25.0);
            tmp = (((u8)(p.x / 2)%2) ^ ((u8)(p.z / 2)%2)) * 1.0;
            // *mat    = (Material){ (Color){ tmp, tmp, tmp }, 1.0, 0.2, 1.0 };
            *mat = (Material){ (Color){ tmp, tmp, tmp },
                               { 1.0, 0.5, 0.0 }, 90.0 };
            *normal = (vec3){ 0, 1, 0 };
            *point  = p;
        }
    }

    return closest < 1000.0;
}

Color raytrace(Canvas* cn, vec3 orig, vec3 dir, int depth);
vec3 vec3_reflect(vec3 dir, vec3 axis) {
    vec3 refdir;
    refdir = vec3_fmul(axis, vec3_dotp(dir, axis) * 2);
    // refdir = vec3_normalize(vec3_sub(dir, refdir));
    refdir = vec3_sub(dir, refdir);
    
    return refdir;
}
Color reflect(Canvas* cn, vec3 orig, vec3 dir, vec3 axis, int depth) {
    vec3 refdir = vec3_reflect(dir, axis);
    refdir = vec3_normalize(refdir);
    return raytrace(cn, orig, refdir, depth); 
}

Color raytrace(Canvas* cn, vec3 orig, vec3 dir, int depth) {
    vec3 point, normal; Material mat, tm0; Color rc, c, refc;
    vec3 ldir; F L = 0.2; F a;
    vec3 tv0, tv1;
    F diffuse_intensity = 0.0;
    F specular_intensity = 0.0;
    
    if (!intersect(cn, orig, dir, &point, &normal, &mat))
        return (Color){ 0, 0, 0 };

    c = mat.color;
    if (depth < 4 && mat.albedo[2] > 0.001) {
        refc = reflect(cn, point, dir, normal, depth + 1);
    }
    for (int i = 0; i < cn->lcount; ++i) {
        ldir = vec3_normalize(vec3_sub(cn->lights[i].pos, point));
        if (!intersect(cn, point, ldir, &tv0, &tv1, &tm0)) {
            a = cn->lights[i].intensity * fmax(0.0,
                                          vec3_dotp(ldir, normal));
            diffuse_intensity += a;
#define INV(v) vec3_fmul(v, -1.0)
            tv0 = vec3_reflect(ldir, normal);
            a = vec3_dotp(tv0, (dir));
            // printf("%lf\n", a);
            specular_intensity += pow(fmax(0.0, a), mat.specular);
        }
    }
    // printf("%lf\n", specular_intensity);
    diffuse_intensity  *= mat.albedo[0];
    specular_intensity *= mat.albedo[1];
    rc = (Color){
        c.r * diffuse_intensity + specular_intensity + refc.r * mat.albedo[2],
        c.g * diffuse_intensity + specular_intensity + refc.g * mat.albedo[2],
        c.b * diffuse_intensity + specular_intensity + refc.b * mat.albedo[2]
    };

    // return (Color){(F)c.r * L, (F)c.g * L, (F)c.b * L};
    return rc;
}

#define SQR(x) ((x)*(x))
void RAYTRACER(Canvas* cn, point pos) {
    // We will trace a gradient depending of the Z position for now...
    vec3 start = get_dirvec(cn, pos);
    vec3 delta = vec3_normalize(start);

    plot_point(cn, pos, raytrace(cn, start, delta, 0));
}

////////////

#define SIZE 400

int main() {
    Color pixels[SIZE*SIZE];
    Sphere spheres[] = {
        // {{-4, 1.5, 30}, 3, {{255, 0, 255}, 1.0, 0.0}},
        // {{5, 3, 20}, 1, {{128, 0, 0}, 1.0, 0.0}},
        {{10, 15, 40},  4, {{1, 0, 0}, {0.8, 1.0, 0.2}, 160.0}},
        {{0, -6, 40},   4, {{1, 1, 1}, {0.0, 1.0, 1.0}, 160.0}},
        {{-10, -6, 40}, 4, {{0, 0, 1}, {0.9, 0.8, 0.0}, 4.0}}
    };
    Light lights[] = {
        // {{0, 5, 60}, 0.5}
        {{0, 50, 20}, 1.0}
    };

    Mesh  meshes[1] = {
    };
    if (!wavefront_load("mesh.obj", meshes + 0, (vec3){10, -8, 20})) {
        puts("epic gaming fail."); return 1;
    }
    Canvas canvas = {
        .cn = pixels,
        .w = SIZE, .h = SIZE,
        .d = 1, .stepsize = 0.2, // FOV of 90 degrees
        .maxDist = 100.0,
        .sph = spheres, .sphcount = 3,
        .lights = lights, .lcount = 1,
        .meshes = meshes, .mcount = 1
    };

    clear_canvas(&canvas);
    for (int y = 0; y < canvas.w; ++y) {
        for (int x = 0; x < canvas.h; ++x) {
            RAYTRACER(&canvas, (point){x, y});
        }
        printf("%d/%d...\r", y, SIZE); fflush(stdout);
    }
    ppm_write_file("out.ppm", &canvas);

    return 0;
}
