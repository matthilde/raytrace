#ifndef _RAYTRACER_H
#define _RAYTRACER_H

#include <stddef.h>

typedef unsigned char u8;
typedef double F;

typedef struct vec3 { F x, y, z; } vec3;
typedef struct Color { F r, g, b; } Color;
typedef struct Material {
    Color color;
    F albedo[3]; // { diffuse, specular, reflect }
    F specular;  // Specular exponent
} Material;

typedef struct Sphere {
    vec3 pos; F radius;
    Material material;
} Sphere;
typedef struct Triangle {
    vec3 a, b, c;
    Material material;
    vec3 normal;
} Triangle;
typedef struct Light  { vec3 pos; F intensity; } Light;
typedef struct Canvas {
    Color* cn; int w, h; F d, stepsize, maxDist;
    Sphere* sph; int sphcount;
    Light*  lights; int lcount;
    struct Mesh* meshes; int mcount;
} Canvas;

typedef struct point { int x, y; } point;

typedef struct Mesh {
    Triangle* triangles; size_t nt; // Number of triangles
    Material* materials; int nm; // Number of materials
    vec3 pos;
} Mesh;

//// vector.c
vec3 vec3_add(vec3 a, vec3 b);
vec3 vec3_sub(vec3 a, vec3 b);
vec3 vec3_mul(vec3 a, vec3 b);
vec3 vec3_fadd(vec3 a, F x);
vec3 vec3_fmul(vec3 a, F x);
vec3 vec3_fdiv(vec3 a, F x);
// dot product
F vec3_dotp(vec3 a, vec3 b);
// cross product
vec3 vec3_crossp(vec3 a, vec3 b);
F vec3_norm(vec3 v);
// normalize
vec3 vec3_normalize(vec3 v);

//// wavefront.c
int wavefront_load(const char* filename, Mesh* m, vec3 pos);
void wavefront_free_mesh(Mesh* m);

#endif
