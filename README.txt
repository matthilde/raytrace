                       __                                   
                      /\ \__                                
 _ __    __     __  __\ \ ,_\  _ __    __      ___     __   
/\`'__\/'__`\  /\ \/\ \\ \ \/ /\`'__\/'__`\   /'___\ /'__`\ 
\ \ \//\ \L\.\_\ \ \_\ \\ \ \_\ \ \//\ \L\.\_/\ \__//\  __/  Raytracer in C
 \ \_\\ \__/.\_\\/`____ \\ \__\\ \_\\ \__/.\_\ \____\ \____\ by matthilde
  \/_/ \/__/\/_/ `/___/> \\/__/ \/_/ \/__/\/_/\/____/\/____/
                    /\___/                                  
                    \/__/                                   

raytrace is a raytracer written in C. I made it to learn more about raytracing.
It lacks a lot of features but it has been a quite fun project to make!

FEATURES
--------

 * Basic material shading (diffuse and specular)
 * Reflection
 * Supports spheres (wow impressive, literally a basic feature)
 * Triangles with Wavefront .obj file support
 * Outputs the result in a ppm file

KNOWN BUGS
----------

 * Weird graphical artifacts with reflections
 * Triangles renders like shit
 * face normals is probably calculated all wrong when loading an .obj file

BUILD INSTRUCTIONS
------------------

$ compile.sh
xxx/400...
[feh shows up]
$ # woo

The script assumes you have gcc and feh installed. However you can modify the
script to use whatever C compiler you want and whatever image viewer you want.

LICENSING
---------

i don't care.
