/*
 * Raytrace by matthilde
 *
 * vector.c
 *
 * Functions necessary for vectors
 */
#include "raytrace.h"
#include <math.h>

vec3 vec3_add(vec3 a, vec3 b) {
    a.x += b.x; a.y += b.y; a.z += b.z;
    return a;
}
vec3 vec3_sub(vec3 a, vec3 b) {
    a.x -= b.x; a.y -= b.y; a.z -= b.z;
    return a;
}
vec3 vec3_mul(vec3 a, vec3 b) {
    a.x *= b.x; a.y *= b.y; a.z *= b.z;
    return a;
}
vec3 vec3_fadd(vec3 a, F x) {
    a.x += x; a.y += x; a.z += x;
    return a;
}
vec3 vec3_fmul(vec3 a, F x) {
    a.x *= x; a.y *= x; a.z *= x;
    return a;
}
vec3 vec3_fdiv(vec3 a, F x) {
    a.x /= x; a.y /= x; a.z /= x;
    return a;
}
// dot product
F vec3_dotp(vec3 a, vec3 b) {
    return (a.x * b.x) + (a.y * b.y) + (a.z * b.z);
}
// cross product
vec3 vec3_crossp(vec3 a, vec3 b) {
    vec3 r;
    r.x = a.y * b.z - a.z * b.y;
    r.y = a.z * b.x - a.x * b.z;
    r.z = a.x * b.y - a.y * b.x;
    return r;
}
F vec3_norm(vec3 v) {
    return sqrt(vec3_dotp(v, v));
}
// normalize
vec3 vec3_normalize(vec3 v) {
    // v / sqrt(v . v)
    return vec3_fdiv(v, sqrt(vec3_dotp(v, v)));
}
